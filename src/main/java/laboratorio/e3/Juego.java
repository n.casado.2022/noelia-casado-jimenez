package laboratorio.e3;

import edu.princeton.cs.algs4.In;

public class Juego {

    private int[][] juegoArr;

    // Construye una clase juego leyéndola del
    //fichero
    public Juego(In ficheroJuego) throws Exception {
        // Inicializamos el array de jugadas 6x6 a valor = -1, para poder validarlo luego
        this.juegoArr = new int[6][6];
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                this.juegoArr[i][j] = -1;
            }
        }

        while (ficheroJuego.hasNextLine()) {
            String linea = ficheroJuego.readLine();
            String[] elem = linea.split(" ");
            if (elem.length == 3) {
                int dado1 = Integer.parseInt(elem[0]);
                int dado2 = Integer.parseInt(elem[1]);
                int valor = Integer.parseInt(elem[2]);
                this.juegoArr[dado1 - 1][dado2 - 1] = valor;
            }
            else {
                throw new Exception("Error al leer fichero juego: número de elementos por línea erróneo");
            }
        }
    }

    // Devuelve si el juego leído es válido
    public boolean esValido() {
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                if (this.juegoArr[i][j] == -1) {
                    return false;
                }
            }
        }
        return true;
    }

    // Devuelve el valor de la tirada t en este
    //Juego
    public int valor(Tirada t) {
        int dado1 = t.getTirada(0);
        int dado2 = t.getTirada(1);

        int valor = this.juegoArr[dado1 - 1][dado2 - 1];
        return valor;
    }
}