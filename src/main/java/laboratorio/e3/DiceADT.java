package laboratorio.e3;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;
import java.util.ArrayList;

public class DiceADT {

    private static final int NUM_JUGADORES_TOP = 10;

    // Recorrer Array buscando jugador con mayor puntuacion
    private static String jugadorMaximaPuntuacion(Juego objJuego , ArrayList<Tirada> tiradasArr) {
        int puntuacionMaxima = 0;
        String tiradorMaximo = null;
        for (Tirada tirada : tiradasArr) {
            int puntuacion = objJuego.valor(tirada);
            if (puntuacion > puntuacionMaxima) {
                tiradorMaximo = tirada.getNombre();
                puntuacionMaxima = puntuacion;
            }
        }
        return tiradorMaximo;
    }

    private static ArrayList<Tirada> leerFicheroTiradas(String tiradasFname) {
        ArrayList<Tirada> tiradasArr = new ArrayList<>();
        In ficheroTiradas = new In (tiradasFname);
        while (ficheroTiradas.hasNextLine()) {
           Tirada tirada = new Tirada (ficheroTiradas);
           if (tirada.esValida()) {
               tiradasArr.add(tirada);
           }
           else {
               StdOut.println("Tirada no válida, datos fichero tiradas incorrectos");
               System.exit(2);
           }
        }
        return tiradasArr;
    }

    public static String[] top(String juegoFname, String tiradasFname) {
        String[] result = new String[NUM_JUGADORES_TOP];
        try {
            // 1 Leer clase juego.txt
            In ficheroJuego = new In(juegoFname);
            Juego objJuego = new Juego(ficheroJuego);
            if (objJuego.esValido()) {
                StdOut.println("Juego válido, continuamos partida");
            }
            else {
                StdOut.println("Juego no válido, datos fichero juego incorrectos");
                System.exit(1);
            }

            // 2 Leer clase tiradas.txt
            ArrayList<Tirada> tiradasArr = DiceADT.leerFicheroTiradas(tiradasFname);

            // 3 Buscamos los 10 jugadores con mayor puntuacion
            for (int i = 0; i < NUM_JUGADORES_TOP; i++) {
                // Buscamos al jugador con mayor puntuacion
                String tiradorMaximo = DiceADT.jugadorMaximaPuntuacion(objJuego, tiradasArr);
                result[i] = tiradorMaximo;

                // Eliminamos todas las tiradas del jugador con maxima puntuacion
                ArrayList<Tirada> tiradasABorrar = new ArrayList<>();

                for (Tirada tirada : tiradasArr) {
                    if (tiradorMaximo.equals(tirada.getNombre())) {
                        tiradasABorrar.add(tirada);
                    }
                }

                for (Tirada t : tiradasABorrar) {
                    tiradasArr.remove(t);
                }
            }
        }
        catch (Exception exception){
            StdOut.println("Excepcion recibida");
        }
        return result;
    }

    public static void main(String[] args) {

        String juegoFname;
        String tiradasFname;

        // 1 Leer argumentos de entrada
        if (args.length == 2) {
            juegoFname = args[0];
            tiradasFname = args[1];

            // 2 Realizar top
            String[] result = DiceADT.top(juegoFname, tiradasFname);

            // 3 Mostrar resultados
            StdOut.println("Resultado top 10 mejores: ");
            for (String s : result) {
                if (s != null) {
                    StdOut.println(s);
                }
            }
        }
        else{
            StdOut.println("Error: parametros de entrada");
        }
    }
}
