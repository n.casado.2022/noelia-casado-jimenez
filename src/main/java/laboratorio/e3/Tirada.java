package laboratorio.e3;

import edu.princeton.cs.algs4.In;

import java.util.Arrays;

public class Tirada {

    private String nombre;
    private int[] dados;

    // Construye una tirada (una tirada son los
    //dados que hay en una línea) leyéndola del
    //fichero
    public Tirada(In ficheroTiradas) {
        this.nombre = ficheroTiradas.readLine();
        this.dados = new int[2];
        this.dados[0] = Integer.parseInt(ficheroTiradas.readLine());
        this.dados[1] = Integer.parseInt(ficheroTiradas.readLine());
    }

    // Devuelve el valor del dado n-ésimo (una
    //Tirada se compone de varias tiradas de
    //dados), la primera es la 0, la segunda la 1
    //y así sucesivamente
    public int getTirada(int nt) {
        if (nt == 0 || nt == 1) {
            return this.dados[nt];
        }
        else {
            throw new ArrayIndexOutOfBoundsException("Error: Solicitud fuera de índice del array dados para jugador " + this.nombre);
        }
     }

    // Devuelve el nombre del jugador de esa
    //tirada
    public String getNombre() {
        return this.nombre;
    }

    // Devuelve si la tirada leída es válida
    public boolean esValida() {
        // Comprobacion 1: Nombre relleno
        if (this.nombre == null) {
            return false;
        }
        // Comprobacion 2: Array Dados inicializado
        if (this.dados == null) {
            return false;
        }
        // Comprobacion 3: Compruebo valor correcto de cada dado
        for (int valor : dados) {
            if (valor < 1 || valor > 6) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tirada tirada = (Tirada) o;
        return getNombre().equals(tirada.getNombre()) && Arrays.equals(dados, tirada.dados);
    }
}



