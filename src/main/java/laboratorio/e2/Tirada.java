package laboratorio.e2;

public class Tirada {

    private String tirador;
    private int dado1;
    private int dado2;

    public Tirada(String tirador, int dado1, int dado2) {
        this.tirador = tirador;
        this.dado1 = dado1;
        this.dado2 = dado2;
    }

    public String tirador() {
        return this.tirador;
    }

    public int dado1() {
        return this.dado1;
    }

    public int dado2() {
        return this.dado2;
    }

    @Override
    public String toString() {
        return "Tirada{" +
                "tirador='" + tirador + '\'' +
                ", dado1=" + dado1 +
                ", dado2=" + dado2 +
                '}';
    }

}
