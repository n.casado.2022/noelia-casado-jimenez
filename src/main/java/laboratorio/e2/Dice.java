package laboratorio.e2;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;
import java.util.ArrayList;

public class Dice {

    private static final int NUM_JUGADORES_TOP = 10;

    // Recorrer Array buscando jugador con mayor puntuacion
    private static String jugadorMaximaPuntuacion(int[][] juegoArr, ArrayList<Tirada> tiradasArr) {
        int puntuacionMaxima = 0;
        String tiradorMaximo = null;
        for (int i = 0; i < tiradasArr.size(); i++) {
            Tirada tirada = tiradasArr.get(i);
            int puntuacion = juegoArr[tirada.dado1() - 1][tirada.dado2() - 1];
            if (puntuacion > puntuacionMaxima) {
                tiradorMaximo = tirada.tirador();
                puntuacionMaxima = puntuacion;
            }
        }
        return tiradorMaximo;
    }

    private static int[][] leerFicheroJuego(String juegoFname) {
        int[][] juegoArr = new int[6][6];
        In juegoIn = new In(juegoFname);
        while (juegoIn.hasNextLine()) {
            String linea = juegoIn.readLine();
            String[] elem = linea.split(" ");
            int n = Integer.parseInt(elem[0]);
            int m = Integer.parseInt(elem[1]);
            int valor = Integer.parseInt(elem[2]);
            juegoArr[n - 1][m - 1] = valor;
        }
        return juegoArr;
    }

    private static ArrayList<Tirada> leerFicheroTiradas(String tiradasFname) {
        ArrayList<Tirada> tiradasArr = new ArrayList<>();
        In tiradasIn = new In (tiradasFname);
        while (tiradasIn.hasNextLine()) {
            String nombre = tiradasIn.readLine();
            int dado1 = Integer.parseInt(tiradasIn.readLine());
            int dado2 = Integer.parseInt(tiradasIn.readLine());

            Tirada tirada = new Tirada (nombre, dado1, dado2);
            tiradasArr.add(tirada);
        }
        return tiradasArr;
    }

    public static String[] top(String juegoFname, String tiradasFname) {

        String[] result = new String[NUM_JUGADORES_TOP];

        // 1 Leer fichero juego.txt
        int[][] juegoArr = Dice.leerFicheroJuego(juegoFname);

        // 2 Leer fichero tiradas.txt
        ArrayList<Tirada> tiradasArr = Dice.leerFicheroTiradas(tiradasFname);

        // 3 Buscamos los 10 jugadores con mayor puntuacion
        for (int i = 0; i < NUM_JUGADORES_TOP; i++) {
            // Buscamos al jugador con mayor puntuacion
            String tiradorMaximo = Dice.jugadorMaximaPuntuacion(juegoArr, tiradasArr);
            result[i] = tiradorMaximo;

            // Eliminamos todas las tiradas del jugador con maxima puntuacion
            for (int j = 0; j < tiradasArr.size(); j++) {
                Tirada tirada = tiradasArr.get(j);
                if (tiradorMaximo.equals(tirada.tirador())) {
                    tiradasArr.remove(j);
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {

        String juegoFname;
        String tiradasFname;

        // 1 Leer argumentos de entrada
        if (args.length == 2) {
            juegoFname = args[0];
            tiradasFname = args[1];

            // 2 Realizar top
            String[] result = Dice.top(juegoFname, tiradasFname);

            // 3 Mostrar resultados
            StdOut.println("Resultado top 10 mejores: ");
            for (int i = 0; i < result.length; i++) {
                if (result[i] != null) {
                    StdOut.println(result[i]);
                }
            }
        }
        else {
            StdOut.println("Error: Parametros de entrada");
        }
    }
}