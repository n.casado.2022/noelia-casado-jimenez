package laboratorio.e1;

import edu.princeton.cs.algs4.StdOut;

public class MyEcho {
    public static void main(String[] args) {

    if (args.length != 0) {
        StdOut.println(args);
    }
    else {
        System.err.println("Error: no args");
    }

    }
}
