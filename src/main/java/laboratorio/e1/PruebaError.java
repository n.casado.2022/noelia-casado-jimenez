package laboratorio.e1;

import edu.princeton.cs.algs4.*;
    
public class PruebaError {
    public static void main(String[] args){
	int x = -3;
	StdOut.println("Programa PruebaError");
	System.err.println("Traza: contenido de x: " + x);
	if (x < 0) {
	    System.err.println("Error: el valor de x no puede ser negativo");
	    System.exit(1);
	}
    }
}
