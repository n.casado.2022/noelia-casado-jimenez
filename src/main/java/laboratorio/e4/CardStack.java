package laboratorio.e4;

public class CardStack {

    // Referencia al nodo que está en la cima de la pila
    private Node top;
    // Número de elementos de la pila
    private int length;

    // Clase para definir los nodos de la lista enlazada
    private class Node {
        private Card card;
        private Node next;

        public Node (Card card) {
            this.card = card;
        }
    }

    // construye una pila vacía
    public CardStack() {
        top = null;
        length = 0;
    }

    // añade un elemento
    public void push(Card card) {
        Node newNode = new Node(card);
        newNode.next = top;
        top = newNode;
        length++;
    }

    // extrae y devuelve la carta más recientemente añadida, null si está vacía
    public Card pop() {
        if (top != null) {
            Card card = top.card;
            top = top.next;
            length--;
            return card;
        }
        return null;
    }

    // devuelve el número de elementos en la pila
    public int length() {
        return length;
    }

    // devuelve representación como string de las cartas de la pila en el orden de la pila (el último más a la derecha)
    public String toString() {
        StringBuilder String = new StringBuilder();
        String.append("[");
        Node node = top;
        while (node != null) {
            String.append(node.card.toString());
            if (node.next != null) {
                String.append(", ");
            }
            node = node.next;
        }
        String.append("]");
        return String.toString();
     }
}
