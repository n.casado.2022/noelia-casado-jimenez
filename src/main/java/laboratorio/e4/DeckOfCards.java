package laboratorio.e4;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

public class DeckOfCards {

    private CardStack stack;

    // Construye un montón vacío
    public DeckOfCards() {
        stack = new CardStack();
    }

    // añade un elemento
    public void push(Card card) {
        stack.push(card);
    }

    // extrae y devuelve la carta más recientemente añadida, null si está vacía
    public Card pop() {
        if (stack != null) {
            return stack.pop();
        }
        return null;
    }

    // Corta el montón por la carta n-ésima. Si hay menos de n (o igual), o si n es menor o igual que cero deja el montón sin cambios
    public void cut(int n) {
        int length = stack.length();
        if (length < n || n <= 0) {
            return;
        }

        CardStack newStack = new CardStack();
        for (int i = n; i < length; i++) {
            newStack.push(stack.pop());
        }

        while (stack.length() > 0) {
            newStack.push(stack.pop());
        }

        stack = newStack;
    }

    // invierte el orden de las cartas en la pila
    public void invert() {
        CardStack newStack = new CardStack();

        while (stack.length() > 0) {
            newStack.push(stack.pop());
        }

        stack = newStack;
    }

    // Corta y mezcla la pila de cartas n veces de forma pseudoaleatoria utilizando la semilla. La mezcla se hace uno sí, uno no de forma alterna
    public void mix(int n, int seed) {
        for (int i = 0; i < n; i++) {
            if (i % 2 == 0) {
                cut (seed % stack.length() + 1);
            }
            else {
                invert();
            }
        }
    }

    // devuelve representación como string de las cartas de la pila en orden de la pila
    public String toString() {
        return stack.toString();
    }

    public static void main(String[] args) {

        DeckOfCards deck = new DeckOfCards();
        String Fichero;

        if (args.length == 1) {
            Fichero = args[0];
            In fichero = new In (Fichero);

            while(fichero.hasNextLine()) {
                String linea = fichero.readLine();
                String[] elem = linea.split(" ");
                String comando = elem[0];

                if (comando.equals("drop")) {
                    int value = Integer.parseInt(elem[1]);
                    char suit = elem[2].charAt(0);

                    deck.push(new Card(value, suit));
                }

                else if (comando.equals("take")) {
                    Card card = deck.pop();

                    if (card != null) {
                        StdOut.println(card.toString());
                    }
                }

                else if (comando.equals("mix")) {
                    int n = Integer.parseInt(elem[1]);
                    int seed = Integer.parseInt(elem[2]);

                    deck.mix(n, seed);
                }

                else if (comando.equals("print")) {
                    StdOut.println(deck.toString());
                }

                else {
                    StdOut.println("Introduce un comando válido: drop, mix, print o take");
                }
            }
        }
        else {
            StdOut.println("Error: Número de parámetros de entrada incorreto");
        }
    }
}
