package laboratorio.e4;

public class Card {
    
    private int value;
    private char suit;

    // Construye una carta. El value será del 1 al 10, el suit de la A a la D
    public Card(int value, char suit) {
        this.value = value;
        this.suit = suit;
    }

    // Dice si una carta es válida
    public boolean isValid() {
        if (value >= 1 && value <= 10) {
            if (suit == 'A') {
                return true;
            }
            if (suit == 'B') {
                return true;
            }
            if (suit == 'C') {
                return true;
            }
            if (suit == 'D') {
                return true;
            }
        }
        return false;
    }
    
    // Devuelve el valor de la carta
    public int getValue() {
        return value;
    }

    // Devuelve el palo de la carta 
    public char getSuit() {
        return suit;
    }
    
    // Devuelve representación como string: "1 D"
    public String toString() {
        return value + " " + suit;
    }

    // Dice si dos cartas son iguales (si su valor y palo son iguales)
    public boolean equals (Object other) {
        Card card = (Card) other;
        if (this.value == card.getValue() && this.suit == card.getSuit()) {
            return true;
        }
        return false;
    }
}
