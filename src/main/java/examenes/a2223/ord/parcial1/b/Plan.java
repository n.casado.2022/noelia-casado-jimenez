package examenes.a2223.ord.parcial1.b;

import java.util.Queue;
import examenes.a2223.ord.parcial1.a.Movement;

public class Plan {

    private String PlanId;
    private static Queue<Movement> Movimientos;
    private double duracionTotal;
    private static int nMovimientos;

    // Construye un plan sin movimientos asociadas con un determinado identificador
    public Plan (String id) {
        PlanId = id;
    }

    // Añade un movimiento al plan
    public void addMovement (Movement movement) {
        Movimientos.add(movement);
    }

    // Devuelve el identificador del plan
    public String getId() {
        return PlanId;
    }

    // Devuelve la duración total del plan, dada por la suma de las duraciones individuales de los movimientos que lo componen
    public double getDuration(){
        for (int i = 0; i < Movimientos.size(); i++) {
            duracionTotal = duracionTotal + getDuration();
        }
        return duracionTotal;
    }

    // Devuelve los movimientos del plan
    public static Queue<Movement> getMovements() {
        for (int i = 0; i < Movimientos.size(); i++) {
            nMovimientos = i++;
        }
        return Movimientos;
    }

    // Devuelve true si todos los movimientos del plan son válidos, y false si alguno no lo es
    public boolean isValid() {
        for (int i = 0; i < Movimientos.size(); i++) {
            if ((getDuration() > 0) && (getMovements() != null)) {
                return true;
            }
        }
        return false;
    }

    // Devuelve true si el el plan actual es igual al objeto other, y false si no lo es
    public boolean equals(Object other) {
        for ( int i = 0; i < Movimientos.size(); i++) {
            Movement otherMovement = (Movement) other;
            if (Movimientos == otherMovement) {
                return true;
            }
        }
        return false;
    }

    //Devuelve representación como string del objeto: [<id>] <número de movimientos> - <duración total>
    public String toString() {
        return "[<" + PlanId + ">]" + " <" + nMovimientos + ">" + " - " + "<" + duracionTotal + ">";
    }
}
