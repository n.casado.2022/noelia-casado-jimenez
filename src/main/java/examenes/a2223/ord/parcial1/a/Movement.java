package examenes.a2223.ord.parcial1.a;

public class Movement {
    // TAD que representa los movimientos de un robot, definidos por una velocidad (en m/s),
    // un ángulo (en radianes) y una duración (en segundos).

    private double speed;
    private double angle;
    private double duration;

    // Construye un movimiento con una determinada velocidad, ángulo y una duración
    public Movement (double speed, double angle, double time) {
        this.speed = speed;
        this.angle = angle;
        this.duration = time;
    }

    // Devuelve la velocidad del movimiento
    public double getSpeed() {
        return speed;
    }

    // Devuelve el ángulo del movimiento
    public double getAngle() {
        return angle;
    }

    // Devuelve la duración del movimiento
    public double getDuration() {
        return duration;
    }

    // Establece la velocidad del movimiento al parámetro speed
    public void setSpeed(double speed) {
        this.speed = speed;
    }

    // Establece el ángulo del movimiento al parámetro angle
    public void setAngle(double angle) {
        this.angle = angle;
    }

    // Establece la duración del movimiento al parámetro duration
    public void setDuration(double duration) {
        this.duration = duration;
    }

    // Devuelve true si el movimiento es válido, y false si no lo es. Para que un movimiento sea válido,
    // su velocidad ha de estar en el rango [-1.5, 1.5], su orientación ha de estar en el rango [-pi/2, pi/2],
    // y la duración ha de ser mayor a 0 segundos
    public boolean isValid() {
        if ((speed <= 1.5 && speed >= -1.5) && (duration > 0) && (angle <= Math.PI/2 && angle >= -Math.PI/2)) {
            return true;
        }
        return false;
    }

    // Devuelve true si el el movimiento actual es igual al objeto other, y false si no lo es
    public boolean equals(Object other) {
        Movement otherMovement = (Movement) other;
        if ((this.speed == otherMovement.getSpeed()) && (this.angle == otherMovement.getAngle()) && (this.duration == otherMovement.getDuration())) {
            return true;
        }
        return false;
    }

    // Devuelve representación como string del objeto: "[<speed> - <angle> - <duration>]
    public String toString() {
        return "[" + "<" + speed + ">" + " - " + "<" + angle + ">" + " - " + "<" + duration + ">" + "]";
    }
}
