package examenes.a2223.ord.parcial1.c;

import edu.princeton.cs.algs4.In;
import examenes.a2223.ord.parcial1.b.Plan;

public class Robot {

    private String nombreRobot;
    private Plan nombrePlan;

    // Construye un robot dados su nombre y un plan para su ejecución
    public Robot (String name, Plan plan) {
        this.nombreRobot = name;
        this.nombrePlan = plan;
    }

    // Devuelve el nombre del robot
    public String getName() {
        return nombreRobot;
    }

    // Devuelve el plan asociado al robot
    public Plan getPlan() {
        return nombrePlan;
    }

    // Simula la ejecución del plan imprimiendo por la salida estándar el nombre del robot, el identificador del plan,
    //y todos los movimientos del plan a ejecutar
    public void runPlan() {
        System.out.println(nombreRobot);
        System.out.println(nombrePlan);
        System.out.println(Plan.getMovements());
    }

    public static void main(String[] args) {
        if (args.length == 2) {
            String nombreRobot = args[0];
            String Fichero = args[1];
            In fichero = new In (Fichero);

            while(fichero.hasNextLine()) {
                String identificador = fichero.readLine();
                String speed = fichero.readLine();
                String angle = fichero.readLine();
                String duration = fichero.readLine();
            }

        }
        else {
            System.out.println("Error en los parámetros de entrada");
        }
    }
}
