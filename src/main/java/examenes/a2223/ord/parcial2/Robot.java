package examenes.a2223.ord.parcial2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;

public class Robot {
    private String name;
    private Deque<Waypoint> route;

    public Robot(String name, ArrayList<Waypoint> wps) {
        this.name = name;
        route = new ArrayDeque<>(wps);
        sortRoute();
    }

    public boolean navigateForward(int n) {
        int count = 0;
        while (count < n && !route.isEmpty()) {
            Waypoint wp = route.pollFirst();
            System.out.println("** " + name + " ** - navigating FORWARD");
            System.out.println(wp.toString());
            count++;
        }
        return count == n;
    }

    public boolean navigateReverse(int n) {
        int count = 0;
        while (count < n && !route.isEmpty()) {
            Waypoint wp = route.pollLast();
            System.out.println("** " + name + " ** - navigating REVERSE");
            System.out.println(wp.toString());
            count++;
        }
        return count == n;
    }

    public String getStringWPs() {
        StringBuilder sb = new StringBuilder();
        for (Waypoint wp : route) {
            sb.append(wp.toString()).append("\n");
        }
        return sb.toString();
    }

    public int getRemainingWPs() {
        return route.size();
    }

    public Deque<Waypoint> getRoute() {
        return route;
    }

    private void sortRoute() {
        ArrayList<Waypoint> tempList = new ArrayList<>(route);
        tempList.sort((wp1, wp2) -> {
            double distance1 = wp1.getDistanceToReference();
            double distance2 = wp2.getDistanceToReference();
            return Double.compare(distance1, distance2);
        });
        route = new ArrayDeque<>(tempList);
    }

    public static void main(String[] args) {
        if (args.length < 5) {
            System.err.println("Usage: java Robot <route_file> <num_waypoints> <robot_x> <robot_y> <direction>");
            return;
        }

        String routeFile = args[0];
        int numWaypoints = Integer.parseInt(args[1]);
        double robotX = Double.parseDouble(args[2]);
        double robotY = Double.parseDouble(args[3]);
        String direction = args[4];

        ArrayList<Waypoint> waypoints = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(routeFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                double x = Double.parseDouble(line);
                double y = Double.parseDouble(br.readLine());
                Waypoint wp = new Waypoint(x, y, robotX, robotY);
                if (wp.isValid()) {
                    waypoints.add(wp);
                } else {
                    System.err.println("Invalid waypoint coordinates: " + wp.toString());
                }
            }
        } catch (IOException e) {
            System.err.println("Error reading route file: " + e.getMessage());
            return;
        }

        Robot robot = new Robot("r2d2", waypoints);
        System.out.println("Route:\n" + robot.getStringWPs());

        boolean navigationSuccess;
        if (direction.equalsIgnoreCase("forward")) {
            navigationSuccess = robot.navigateForward(numWaypoints);
        } else if (direction.equalsIgnoreCase("reverse")) {
            navigationSuccess = robot.navigateReverse(numWaypoints);
        } else {
            System.err.println("Invalid direction. Valid options: forward, reverse");
            return;
        }

        if (navigationSuccess) {
            if (robot.getRemainingWPs() == 0) {
                System.out.println("The robot reached the final WP");
            } else {
                System.out.println("Points remaining:\n" + robot.getStringWPs());
            }
        } else {
            System.err.println("Navigation error");
        }
    }
}
