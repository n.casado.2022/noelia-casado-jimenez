package examenes.a2223.ord.parcial2;

public class Waypoint {
    private double x;
    private double y;
    private double originX;
    private double originY;
    private double distanceToOrigin;
    private boolean updated;

    public Waypoint(double x, double y, double o_x, double o_y) {
        this.x = x;
        this.y = y;
        this.originX = o_x;
        this.originY = o_y;
        calculateDistanceToOrigin();
        updated = false;
    }

    public Waypoint(double x, double y) {
        this(x, y, 0, 0);
    }

    public double getDistanceToReference() {
        return distanceToOrigin;
    }

    public String updateReference(double o_x, double o_y) {
        originX = o_x;
        originY = o_y;
        calculateDistanceToOrigin();
        updated = true;
        return toString();
    }

    public boolean hasBeenUpdated() {
        return updated;
    }

    public boolean isValid() {
        return isValidCoordinate(x) && isValidCoordinate(y) && isValidCoordinate(originX) && isValidCoordinate(originY);
    }

    private boolean isValidCoordinate(double coordinate) {
        return coordinate >= 0 && coordinate <= 1;
    }

    private void calculateDistanceToOrigin() {
        double deltaX = x - originX;
        double deltaY = y - originY;
        distanceToOrigin = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
    }

    public String toString() {
        return "[" + x + ", " + y + "]: " + distanceToOrigin;
    }
}