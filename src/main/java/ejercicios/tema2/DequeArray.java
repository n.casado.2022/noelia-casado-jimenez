package ejercicios.tema2;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class DequeArray<Item> implements Iterable<Item> {
    private static final int INITIAL_CAPACITY = 8;
    private Item[] array;
    private int size;

    public DequeArray() {
        array = (Item[]) new Object[INITIAL_CAPACITY];
        size = 0;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }

    public int mem() {
        return array.length;
    }

    public void pushLeft(Item item) {
        if (size == array.length) {
            resize(2 * array.length);
        }

        for (int i = size; i > 0; i--) {
            array[i] = array[i - 1];
        }

        array[0] = item;
        size++;
    }

    public void pushRight(Item item) {
        if (size == array.length) {
            resize(2 * array.length);
        }

        array[size] = item;
        size++;
    }

    public Item popLeft() {
        if (isEmpty()) {
            return null;
        }

        Item item = array[0];

        for (int i = 1; i < size; i++) {
            array[i - 1] = array[i];
        }

        size--;
        array[size] = null;

        if (size > 0 && size == array.length / 4) {
            resize(array.length / 2);
        }

        return item;
    }

    public Item popRight() {
        if (isEmpty()) {
            return null;
        }

        Item item = array[size - 1];
        size--;
        array[size] = null;

        if (size > 0 && size == array.length / 4) {
            resize(array.length / 2);
        }

        return item;
    }
    
    private void resize(int capacity) {
        Item[] newArray = (Item[]) new Object[capacity];

        for (int i = 0; i < size; i++) {
            newArray[i] = array[i];
        }

        array = newArray;
    }

    public Iterator<Item> iterator() {
        return new DequeIterator();
    }

    public Iterator<Item> reverseIterator() {
        return new ReverseDequeIterator();
    }

    private class DequeIterator implements Iterator<Item> {
        private int currentIndex;

        public DequeIterator() {
            currentIndex = 0;
        }

        public boolean hasNext() {
            return currentIndex < size;
        }

        public Item next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            Item item = array[currentIndex];
            currentIndex++;
            return item;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    private class ReverseDequeIterator implements Iterator<Item> {
        private int currentIndex;

        public ReverseDequeIterator() {
            currentIndex = size - 1;
        }

        public boolean hasNext() {
            return currentIndex >= 0;
        }

        public Item next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            Item item = array[currentIndex];
            currentIndex--;
            return item;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < size; i++) {
            sb.append(array[i]);
            if (i < size - 1) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    public static void main(String[] args) {
        DequeArray<Integer> deque = new DequeArray<>();
        deque.pushLeft(1);
        deque.pushRight(2);
        deque.pushLeft(3);
        deque.pushRight(4);

        System.out.println("Deque: " + deque.toString()); // [3, 1, 2, 4]

        System.out.println("Pop Left: " + deque.popLeft()); // 3
        System.out.println("Pop Right: " + deque.popRight()); // 4

        System.out.println("Deque after pops: " + deque.toString()); // [1, 2]

        System.out.println("Size: " + deque.size()); // 2
        System.out.println("Memory: " + deque.mem()); // 8
    }
}