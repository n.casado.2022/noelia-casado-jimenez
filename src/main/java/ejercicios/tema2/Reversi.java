package ejercicios.tema2;

import edu.princeton.cs.algs4.In;
import java.util.Stack;

public class Reversi {
    private static Stack<String> pila;  // Pila de operaciones
    private static Tablero tablero;    // Tablero del juego

    // Constructor de Reversi
    public Reversi() {
        pila = new Stack<>();
        tablero = new Tablero();
    }

    // Método para obtener el tablero del juego
    public Tablero getTablero() {
        return tablero;
    }

    // Método para poner una ficha en la posición indicada y color indicado
    public static void put(String fila, String columna, String color) {
        int f = Integer.parseInt(fila);
        int cl = Integer.parseInt(columna);
        char cr = color.charAt(0);
        char[][] conf = tablero.getConf();
        if (conf[f - 1][cl - 1] != 'B' &&  conf[f - 1][cl - 1] != 'W') {
            conf[f - 1][cl - 1] = cr;
        }
        else {
            System.err.println("Error: Posición ya ocupada");
        }
    }

    // Método para dar la vuelta a una ficha
    public static boolean turn(String fila, String columna) {
        int f = Integer.parseInt(fila);
        int cl = Integer.parseInt(columna);
        char[][] conf = tablero.getConf();
        char a = conf[f - 1][cl - 1];
        if (a == 'W') {
            conf[f - 1][cl - 1] = 'B';
        }
        else if (a == 'B') {
            conf[f - 1][cl - 1] = 'W';
        }
        return false;
    }

    // Método para contar las fichas blancas y negras que hay
    public static void count() {
        int B = 0;
        int W = 0;
        char[][] conf = tablero.getConf();
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (conf[i][j] == 'W') {
                    W = W + 1;
                }
                else if (conf[i][j] == 'B') {
                    B = B + 1;
                }
            }
        }
        System.out.println("W: " + W);
        System.out.println("B: " + B);
    }

    // Método para deshacer la operacion anterior a undo
    public static void undo() {
        if (!pila.isEmpty()) {
            String last = pila.pop();
            if (last.equals("Turn") || last.equals("Put")) {
                System.out.println("Se deshace la operación" + last);
            }
            else {
                pila.push(last);
            }
        }
    }

    public static void main(String[] args) {
        if (args.length == 1) {
            Stack<String> pila = new Stack<>();
            String archivo = args[0];
            In operaciones = new In(archivo);
            while (operaciones.hasNextLine()) {
                String linea = operaciones.readLine();
                String[] elem = linea.split(" ");
                pila.push(elem[0]);
                String next = operaciones.readLine();
                if (next != "Undo") {
                    if (elem[0] == "Turn") {
                        turn(elem[1], elem[2]);
                    }
                    else if (elem[0] == "Put") {
                        put(elem[1], elem[2], elem[3]);
                    }
                    else if (elem[0] == "Count") {
                        count();
                    }
                }
                else {
                    if (elem[0] == "Count") {
                        count();
                    }
                    else {
                        pila.pop();
                    }
                }
            }
        }
        else {
            System.err.println("Error: Debe introducir un único fichero de instrucciones");
        }
    }
}