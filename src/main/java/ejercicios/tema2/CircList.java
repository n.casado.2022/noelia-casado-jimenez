package ejercicios.tema2;

import java.util.Iterator;
import java.util.function.Consumer;

public class CircList<Item> implements Iterable<Item> {

    private Node cursor;
    private int size;

    private class Node {
        Item item;
        Node next;
        Node prev;
    }

    // crea un objeto CircList vacío
    CircList() {
        cursor = null;
        size = 0;
    }

    // ¿está vacía?
    boolean isEmpty() {
        return size == 0;
    }

    // devuelve el número de elementos
    int size() {
        return size;
    }

    // introduce un elemento en el siguiente al cursor
    void insert(Item item) {
        Node newNode = new Node();
        newNode.item = item;
        if(isEmpty()) {
            cursor = newNode;
            newNode.prev = newNode;
            newNode.next = newNode;
        }
        else {
            newNode.prev = cursor;
            newNode.next = cursor.next;
            cursor.next.prev = newNode;
            cursor.next = newNode;
        }
        size++;
    }

    // devuelve el elemento en el que está el cursor
    Item cursor() {
        if (isEmpty()) {
            return null;
        }
        return cursor.item;
    }

    // devuelve el elemento en el que está el cursor y avanza el cursor al siguiete elemento
    Item forwardCursor() {
        if (isEmpty()) {
            return null;
        }
        cursor = cursor.next;
        return cursor.prev.item;
    }

    // extrae el elemento del cursor, y avanza al siguiente
    Item popCursor() {
        if (isEmpty()) {
            return null;
        }
        Item item = cursor.item;
        if (size == 1) {
           cursor = null;
        }
        else {
            cursor.next.prev = cursor.prev;
            cursor.prev.next = cursor.next;
            cursor = cursor.next;
        }
        size--;
        return item;
    }

    // devuelve un iterador que recorre la CircList de forma circular hasta volver al cursor
    public Iterator<Item> iterator() {
        return new CircListIterator();
    }

    private class CircListIterator implements Iterator<Item> {
        private Node current = cursor;

        public boolean hasNext() {
            return current != null && (current != cursor || current.next != cursor);
        }

        public Item next() {
            if (!hasNext()) {
                return null;
            }
            current = current.next;
            return current.item;
        }

        @Override
        public void forEachRemaining(Consumer<? super Item> action) {
            Iterator.super.forEachRemaining(action);
        }
    }
}