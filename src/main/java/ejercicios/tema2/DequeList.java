package ejercicios.tema2;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class DequeList<Item> implements Iterable<Item> {
    private Node first; // primer nodo de la deque
    private Node last; // último nodo de la deque
    private int size; // número de elementos en la deque

    private class Node {
        Item item;
        Node next;
        Node previous;
    }

    public DequeList() {
        first = null;
        last = null;
        size = 0;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }

    public int mem() {
        return size; // Se asume que cada elemento ocupa aproximadamente dos referencias de memoria
    }

    public void pushLeft(Item item) {
        Node newNode = new Node();
        newNode.item = item;

        if (isEmpty()) {
            first = newNode;
            last = newNode;
        } else {
            newNode.next = first;
            first.previous = newNode;
            first = newNode;
        }

        size++;
    }

    public void pushRight(Item item) {
        Node newNode = new Node();
        newNode.item = item;

        if (isEmpty()) {
            first = newNode;
            last = newNode;
        } else {
            newNode.previous = last;
            last.next = newNode;
            last = newNode;
        }

        size++;
    }

    public Item popLeft() {
        if (isEmpty()) {
            return null;
        }

        Item item = first.item;
        first = first.next;

        if (first == null) {
            last = null;
        } else {
            first.previous = null;
        }

        size--;

        return item;
    }

    public Item popRight() {
        if (isEmpty()) {
            return null;
        }

        Item item = last.item;
        last = last.previous;

        if (last == null) {
            first = null;
        } else {
            last.next = null;
        }

        size--;

        return item;
    }

    public Iterator<Item> iterator() {
        return new DequeIterator();
    }

    private class DequeIterator implements Iterator<Item> {
        private Node current = first;

        public boolean hasNext() {
            return current != null;
        }

        public Item next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            Item item = current.item;
            current = current.next;
            return item;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        Node current = first;
        while (current != null) {
            sb.append(current.item);
            if (current.next != null) {
                sb.append(", ");
            }
            current = current.next;
        }
        sb.append("]");
        return sb.toString();
    }

    public static void main(String[] args) {
        DequeList<Integer> deque = new DequeList<>();
        deque.pushLeft(1);
        deque.pushRight(2);
        deque.pushLeft(3);
        deque.pushRight(4);

        System.out.println("Deque: " + deque.toString()); // [3, 1, 2, 4]

        System.out.println("Pop Left: " + deque.popLeft()); // 3
        System.out.println("Pop Right: " + deque.popRight()); // 4

        System.out.println("Deque after pops: " + deque.toString()); // [1, 2]

        System.out.println("Size: " + deque.size()); // 2
        System.out.println("Memory: " + deque.mem()); // 4
    }
}