package ejercicios.tema2;

public class Tablero {
    // Configuración del tablero
    private char[][] conf;

    // Constructor del tablero
    public Tablero() {
        conf = new char[8][8];
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                conf[i][j] = '-';
            }
        }
    }

    // Método para obtener la configuración actual del tablero
    public char[][] getConf() {
        return conf;
    }

    public String toString() {
        return conf.toString();
    }
}
