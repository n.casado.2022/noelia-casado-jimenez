package ejercicios.tema4;

import edu.princeton.cs.algs4.In;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortRectangles {

    public static void main(String[] args) {
        if (args.length == 1) {
            List<ComparableRectangle> rectangles = new ArrayList<>();
            String fichero = args[0];
            In txt = new In(fichero);

            while (txt.hasNextLine()) {
                String[] elem = txt.readLine().split(" ");
                if (elem.length != 4) {
                    System.err.println("Data source error");
                    System.exit(1);
                }
                double izqX = 0;
                double izqY = 0;
                double dchX = 0;
                double dchY = 0;

                try {
                    izqX = Double.parseDouble(elem[0]);
                    izqY = Double.parseDouble(elem[1]);
                    dchX = Double.parseDouble(elem[2]);
                    dchY = Double.parseDouble(elem[3]);
                } catch (NumberFormatException n) {
                    System.err.println("Data source error");
                    System.exit(1);
                }

                if (0.0 <= izqX && izqX <= 1.0) {
                    if (0.0 <= izqY && izqY <= 1.0) {
                        if (0.0 <= dchX && dchX <= 1.0) {
                            if (0.0 <= dchY && dchY <= 1.0) {
                                if ((izqX < dchX) && (izqY < dchY)) {
                                    ComparableRectangle rectangle = new ComparableRectangle(izqX, izqY, dchX, dchY);
                                    rectangle.toString();
                                    rectangles.add(rectangle);
                                }
                                else { System.err.println("Rectangle will not be added to the list.\n"); }
                            }
                            else { System.err.println("Rectangle will not be added to the list.\n"); }
                        }
                        else { System.err.println("Rectangle will not be added to the list.\n"); }
                    }
                    else { System.err.println("Rectangle will not be added to the list.\n"); }
                }
                else {
                    System.err.println("Rectangle will not be added to the list.\n");
                }
            }

            Collections.sort(rectangles);
            for (ComparableRectangle rectangle: rectangles) {
                System.out.println(rectangle);
            }
        }
        else {
            System.err.println("Error: Wrong number of input files");
        }
    }
}
