package ejercicios.tema4;

public class ComparableRectangle implements Comparable<ComparableRectangle> {

    private double izqX, izqY, dchX, dchY;

    public ComparableRectangle(double izqX, double izqY, double dchX, double dchY) {
        this.izqX = izqX;
        this.izqY = izqY;
        this.dchX = dchX;
        this.dchY = dchY;
    }

    public double getArea() {
        return Math.abs((dchX - izqX) * (dchY - izqY));
    }

    public int compareTo(ComparableRectangle other) {
        double thisArea = this.getArea();
        double otherArea = other.getArea();
        if (thisArea < otherArea) {
            return -1;
        }
        else if (thisArea > otherArea) {
            return 1;
        }
        else {
            return 0;
        }
    }

    public String toString() {
        String izqX = Double.toString(this.izqX);
        String izqY = Double.toString(this.izqY);
        String dchX = Double.toString(this.dchX);
        String dchY = Double.toString(this.dchY);
        return izqX + ", " + izqY + ", " + dchX + ", " + dchY;
    }
}
