package ejercicios.tema4;

import edu.princeton.cs.algs4.In;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortRectangles2 {

    public static void main(String[] args) {
        if (args.length == 1) {
            List<ComparableRectangle2> rectangles2 = new ArrayList<>();
            String fichero = args[0];
            In txt = new In(fichero);
            while (txt.hasNextLine()) {
                String[] elem = txt.readLine().split(" ");

                if (elem.length != 4) {
                    System.err.println("Data source error");
                    System.exit(1);
                }
                double izqX = 0;
                double izqY = 0;
                double dchX = 0;
                double dchY = 0;

                try {
                    izqX = Double.parseDouble(elem[0]);
                    izqY = Double.parseDouble(elem[1]);
                    dchX = Double.parseDouble(elem[2]);
                    dchY = Double.parseDouble(elem[3]);
                }
                catch (NumberFormatException n) {
                    System.err.println("Data source error");
                    System.exit(1);
                }

                if (0.0 <= izqX && izqX <= 1.0) {
                    if (0.0 <= izqY && izqY <= 1.0) {
                        if (0.0 <= dchX && dchX <= 1.0) {
                            if (0.0 <= dchY && dchY <= 1.0) {
                                if ((izqX < dchX) && (izqY < dchY)) {
                                    ComparableRectangle2 rectangle2 = new ComparableRectangle2(izqX, izqY, dchX, dchY);
                                    rectangle2.toString();
                                    rectangles2.add(rectangle2);
                                }
                                else { System.err.println("Rectangle will not be added to the list.\n"); }
                            }
                            else { System.err.println("Rectangle will not be added to the list.\n"); }
                        }
                        else { System.err.println("Rectangle will not be added to the list.\n"); }
                    }
                    else { System.err.println("Rectangle will not be added to the list.\n"); }
                }
                else {
                    System.err.println("Rectangle will not be added to the list.\n");
                }
            }

            // Sort by area
            Collections.sort(rectangles2, new Comparator<ComparableRectangle2>() {
                public int compare(ComparableRectangle2 r1, ComparableRectangle2 r2) {
                    double area1 = r1.getArea();
                    double area2 = r2.getArea();
                    if (area1 < area2) return -1;
                    else if (area1 > area2) return 1;
                    else return 0;
                }
            });
            System.out.println("BY AREA");
            for (ComparableRectangle2 rect : rectangles2) {
                System.out.println(rect + " - area=" + rect.getArea());
            }

            // Sort by height
            Collections.sort(rectangles2, new Comparator<ComparableRectangle2>() {
                public int compare(ComparableRectangle2 r1, ComparableRectangle2 r2) {
                    double height1 = r1.getHeight();
                    double height2 = r2.getHeight();
                    if (height1 < height2) return -1;
                    else if (height1 > height2) return 1;
                    else return 0;
                }
            });
            System.out.println("BY HEIGHT");
            for (ComparableRectangle2 rect : rectangles2) {
                System.out.println(rect + " - height=" + rect.getHeight());
            }

            // Sort by width
            Collections.sort(rectangles2, new Comparator<ComparableRectangle2>() {
                public int compare(ComparableRectangle2 r1, ComparableRectangle2 r2) {
                    double width1 = r1.getWidth();
                    double width2 = r2.getWidth();
                    if (width1 < width2) return -1;
                    else if (width1 > width2) return 1;
                    else return 0;
                }
            });
            System.out.println("BY WIDTH");
            for (ComparableRectangle2 rect : rectangles2) {
                System.out.println(rect + " - width=" + rect.getWidth());
            }

        }
        else {
            System.err.println("Error: Wrong number of input files");
        }
    }
}
