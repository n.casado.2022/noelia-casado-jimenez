package laboratorio.e2;

import java.util.Arrays;


import static org.junit.Assert.assertEquals;

import org.junit.*;

import java.io.*;
import org.junit.rules.Timeout;


import org.junit.rules.TestWatcher;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

public class DiceTest {
    @Rule
    public Timeout globalTimeout = new Timeout(80000);


    public void ioTest(boolean fast, String comentario, String esperado, String[] args) {
        System.out.println("\n"+ comentario + "\nEsperado:");
        System.out.print(esperado);
        System.out.print("----");
        System.out.println("\nEncontrado:");
        Dice.main(args);

        System.out.println("----");
    }

    static public class TestState{
        BufferedWriter grade;
        TestState() {
            try{
                grade = new BufferedWriter(new FileWriter("results/res.e2", false));
                //grade.write("@@@CLAUSES (TestBasic min 0 this 0) (TestFunc min -4.0 this -4.0)  (ioTests max 1 this -1)\n");
                //grade.write("@@@\tTestBasic\t5\n");
                grade.write("@@@\tMáxima nota: \t5\n");
            }catch (IOException e) {
                AssertionError ae = new AssertionError(e);
                throw ae;
            }
        }
        public void done() {
            try{
                grade.close();
            }catch (IOException e) {
                AssertionError ae = new AssertionError(e);
                throw ae;
            }
        }
        public void outGrade(String region, double g) {
            try{
                grade.write("@@@ "+"\t" + region + "\t" + g +" \n");
            }catch (IOException e) {
                AssertionError ae = new AssertionError(e);
                throw ae;
            }
        }
    }
    static TestState testSt;
    @BeforeClass
    public static void beforeTest() {
        testSt = new TestState();
    }
    @AfterClass
    public static void afterTest() {
        testSt.done();
    }


    @Rule
    public TestRule testWatcher = new TestWatcher() {
        @Override
        public Statement apply(Statement base, Description description) {
            return super.apply(base, description);
        }


        @Override
        protected void failed(Throwable e, Description description) {
            testSt.outGrade(description.getMethodName(), -1.0);
            /*switch(description.getMethodName()){
                case "TestDice_SubAll":
                case "TestDice_Sub1":
                case "TestDice_Big":
                case "TestDice_2":
                    testSt.outGrade("TestFunc", -1.0);
                    break;
                case "TestDice_0":
                    System.out.print("Dice no sale con error?");
                    testSt.outGrade("TestFunc", -1.0);
                default:
                    break;
            }*/
        }
    };

    @Test
    public void TestDice_Big() {
        String comentario = "prueba variada, argumentos: Dice tiradas.txt juego.txt";
        String tiradas = "resources/e2/tiradas.txt";
        String juego = "resources/e2/juego.txt";

        String esperado[] = {"pepe 1", "alberto 3", "francisco", "juan", "pepe", "alberto", "Pepe 2", "pepe 5", "ty", "oligardo"};
        String[] top = Dice.top(juego, tiradas);
        Arrays.sort(top);
        Arrays.sort(esperado);
        Assert.assertEquals(comentario, Arrays.toString(esperado), Arrays.toString(top));
    }

    @Test
    public void TestDice_Sub1() {
        String comentario = "sustituye uno, argumentos: Dice tiradas3.txt juego.txt";
        String tiradas = "resources/e2/tiradas3.txt";
        String juego = "resources/e2/juego.txt";

        String esperado[] = {"pepe 1", "alberto 3", "javier", "ruben", "pedro", "pepe", "rigoberto", "javier rod.", "phobos", "desmos"};
        String[] top = Dice.top(juego, tiradas);
        Arrays.sort(top);
        Arrays.sort(esperado);
        Assert.assertEquals(comentario, Arrays.toString(esperado), Arrays.toString(top));
    }
    @Test
    public void TestDice_SubAll() {
        String comentario = "sustituye uno, argumentos: Dice tiradas4.txt juego.txt";
        String tiradas = "resources/e2/tiradas4.txt";
        String juego = "resources/e2/juego.txt";

        String esperado[] = {"x 1", "x 2", "x 3", "x 4", "x 5", "x 6", "x 7", "x 8", "x 9", "x 10"};
        String[] top = Dice.top(juego, tiradas);
        Arrays.sort(top);
        Arrays.sort(esperado);
        Assert.assertEquals(comentario, Arrays.toString(esperado), Arrays.toString(top));
    }
    @Test
    public void TestDice_2() {
        String comentario = "dos tiradas: argumentos: Dice tiradas2.txt juego.txt";
        String tiradas = "resources/e2/tiradas2.txt";
        String juego = "resources/e2/juego.txt";

        String esperado = "pepe 1\nalberto 3\n";
        String[] args = {tiradas, juego};

        ioTest(false, comentario, esperado, args);
    }


    @Test
    public void TestDice_0() {

        String comentario = "Cero argumentos: Dice";

        String esperado = "usage: dice tiradas.txt juego.txt\n";
        String[] args = {};

        ioTest(false, comentario, esperado, args);
    }
}