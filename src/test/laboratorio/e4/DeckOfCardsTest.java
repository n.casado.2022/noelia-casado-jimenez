package laboratorio.e4;


import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertArrayEquals;

import org.junit.*;

import java.io.*;
import org.junit.rules.Timeout;


import org.junit.rules.TestWatcher;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;


;


public class DeckOfCardsTest {
    @Rule
    public Timeout globalTimeout = new Timeout(80000);


    public void ioTest(boolean fast, String comentario, String esperado, String[] args) {
        System.out.println("\n"+ comentario + "\nEsperado:");
        System.out.print(esperado);
        System.out.print("----");
        System.out.println("\nEncontrado:");
        DeckOfCards.main(args);

        System.out.println("----");
    }

    static public class TestState{
        BufferedWriter grade;
        TestState() {
            try{
                grade = new BufferedWriter(new FileWriter("results/res.e4", false));
                //grade.write("@@@CLAUSES (TestBasic min 0 this 0) (TestFunc min -4.0 this -4.0)  (ioTests max 1 this -1)\n");
                //grade.write("@@@\tTestBasic\t5\n");
                grade.write("@@@\tMáxima nota: \t8\n");
            }catch (IOException e) {
                AssertionError ae = new AssertionError(e);
                throw ae;
            }
        }
        public void done() {
            try{
                grade.close();
            }catch (IOException e) {
                AssertionError ae = new AssertionError(e);
                throw ae;
            }
        }
        public void outGrade(String region, double g) {
            try{
                grade.write("@@@ "+"\t" + region + "\t" + g + "\n");
            }catch (IOException e) {
                AssertionError ae = new AssertionError(e);
                throw ae;
            }
        }
    }
    static TestState testSt;

    @BeforeClass
    public static void beforeTest() {
        testSt = new TestState();
    }
    @AfterClass
    public static void afterTest() {
        testSt.done();
    }


    @Rule
    public TestRule testWatcher = new TestWatcher() {
        @Override
        public Statement apply(Statement base, Description description) {
            return super.apply(base, description);
        }


        @Override
        protected void failed(Throwable e, Description description) {
            testSt.outGrade(description.getMethodName(), -1.0);
            /*switch(description.getMethodName()){
                case "TestCard":
                case "TestCardInvalid":
                case "TestCardStack":
                case "TestCardPileInvert":
                case "TestCardPileCut":
                case "TestCardPileMixSeed":
                case "TestCardPileMix":
                case "TestMain":
                    testSt.outGrade("TestFunc", -1.0);
                    break;
                default:
                    break;
            }*/
        }
    };


    @Test
    public void TestCard() {
        for(byte i = 1; i <= 10; i++){
            char v = (char)('A'+ i % 4);
            Card card = new Card(i, v);
            Card card2 = new Card(i, v);
            Assert.assertTrue("should be valid " + i + "" + v, card.isValid());
            Assert.assertEquals("Card suit should be equal", card.getSuit(), card2.getSuit());
            Assert.assertEquals("Card value should be equal", card.getValue(), card2.getValue());
            Assert.assertEquals("Cards should be equal", card, card2);
        }

    }
    @Test
    public void TestCardInvalid() {
        for(byte i = 1; i <= 15; i++){
            char v = ((char)('E'+i%4));
            Card card = new Card(i, v);
            Assert.assertTrue("should be invalid " + i + ", " + v + " ", !card.isValid());
        }

    }

    public static final int nCardsBig = 17;
    public void pushNUp(DeckOfCards cp, int n) {
        for(byte i = 1; i < n; i++){

            char v =  (char)('A' + i % 4);
            Card card = new Card(i % 10 + 1, v);
            if(!card.isValid()) {
                System.err.println("bad card, should be valid: " + card);
                System.exit(1);
            }
            cp.push(card);
        }
    }
    public void pushNDown(DeckOfCards cp, int n) {

        for(int i = n - 1; i >= 1; i--){
            char v =  (char)('A'+i%4);
            Card card = new Card(i%10 + 1, v);
            if(!card.isValid()) {
                System.err.println("bad card, should be valid: "+card);
                System.exit(1);
            }
            cp.push(card);
        }
    }
    @Test

    public void TestCardStack() {

        CardStack cs = new CardStack();

        for(int tot = 1; tot < 32; tot++) {
            for(byte i = 0; i < tot; i++){
                char v =  (char)('A' + i % 4);
                Card card = new Card(1 + (i % 10), v);
                cs.push(card);
            }
            String cardStr = ", before: "+ cs + ":";
            for(int i = tot - 1; i >= 0; i--){
                char v =  (char)('A' + i % 4);
                Card card = new Card(1 + (i % 10), v);
                Card cardx;
                cardx = cs.pop();
                Assert.assertEquals("Cards should be equal" + cardStr+ " popped "+i+" ", card, cardx);
            }
        }

    }
    @Test
    public void TestInversion() {
        DeckOfCards cp = new DeckOfCards();
        pushNUp(cp, nCardsBig);
        cp.invert();
        DeckOfCards cpi = new DeckOfCards();
        pushNDown(cpi, nCardsBig);
        Assert.assertEquals("invert does not work", cpi.toString(), cp.toString());
    }

    @Test
    public void TestCut() {
        DeckOfCards cp = new DeckOfCards();
        pushNUp(cp, nCardsBig);
        String before = cp.toString();
        cp.cut(5);
        Assert.assertEquals("cutting " + before+" is wrong:", "[7 C, 8 D, 9 A, 10 B, 1 C, 2 D, 3 A, 4 B, 5 C, 6 D, 7 A, 2 B, 3 C, 4 D, 5 A, 6 B]", cp.toString());
    }

    @Test
    public void TestMixSeed() {
        for(int seed = 0; seed < 10; seed++){

            DeckOfCards cp = new DeckOfCards();
            DeckOfCards cp2 = new DeckOfCards();
            pushNUp(cp, nCardsBig);
            pushNUp(cp2, nCardsBig);

            cp.mix(5, seed);
            cp2.mix(5, seed);
            for(byte i = 1; i < nCardsBig; i++){
                Card c1 = cp.pop();
                Card c2 = cp2.pop();
                Assert.assertEquals("same seed should give the same result after mix:  " + cp + "!=" + cp2, c1, c2);
            }
            Assert.assertEquals("same seed should give the same result after mix (after toString): ", cp.toString(), cp2.toString());
        }
    }
    @Test
    public void TestMix() {
        int minNeq = 0;
        String before = "";
        String after = "";
        for(int seed = 0; seed < 40; seed++){
            DeckOfCards cp = new DeckOfCards();
            pushNUp(cp, nCardsBig);
            String bef = cp.toString();
            cp.mix(100,seed);
            String af = cp.toString();
            int neq = 0;
            for(byte i = 1; i < nCardsBig; i++){
                Card c2 = new Card(i%10 + 1, (char)('A'+i%4));
                Card c1 = cp.pop();
                if(c1.equals(c2)){
                    neq++;
                }
            }
            if(minNeq < neq) {
                minNeq = neq;
                before = bef;
                after = af;
            }
        }
        Assert.assertTrue("too many equal cards, bad mix" + before + "->" + after + "equal: " + minNeq, minNeq < 10);
    }
    @Test
    public void TestMain() {
        String fichName = "resources/e4/fichtest.txt";
        String[] args = {fichName};
        ioTest(false, "fichero fichtest.txt", "[]\n[1 A, 1 B, 1 C, 2 D, 1 B, 2 A]\n[1 A, 1 B, 1 C, 2 D, 1 B]\n", args);
        String fichName2 = "resources/e4/fichtest2.txt";
        String[] args2 = {fichName2};
        ioTest(false, "fichero fichtest.txt2", "[1 A]\n", args2);
    }
}
