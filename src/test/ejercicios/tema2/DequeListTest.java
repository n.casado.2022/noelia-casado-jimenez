package ejercicios.tema2;

import laboratorio.e3.DiceADT;
import org.junit.*;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.rules.Timeout;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;


public class DequeListTest {
    @Rule
    public Timeout globalTimeout = new Timeout(80000);


    public void ioTest(boolean fast, String comentario, String esperado, String[] args) {
        System.out.println("\n"+ comentario + "\nEsperado:");
        System.out.print(esperado);
        System.out.print("----");
        System.out.println("\nEncontrado:");
        DiceADT.main(args);

        System.out.println("----");
    }

    static public class TestState{
        BufferedWriter grade;
        TestState() {
            try{
                grade = new BufferedWriter(new FileWriter("results/res.DequeList", false));
                grade.write("@@@\tMáxima nota: \t5\n");
            }catch (IOException e) {
                AssertionError ae = new AssertionError(e);
                throw ae;
            }
        }
        public void done() {
            try{
                grade.close();
            }catch (IOException e) {
                AssertionError ae = new AssertionError(e);
                throw ae;
            }
        }
        public void outGrade(String region, double g) {
            try{
                grade.write("@@@ "+"\t"+region+"\t"+g+"\n");
            }catch (IOException e) {
                AssertionError ae = new AssertionError(e);
                throw ae;
            }
        }
    }
    static TestState testSt;
    @BeforeClass
    public static void beforeTest() {
        testSt = new TestState();
    }
    @AfterClass
    public static void afterTest() {
        testSt.done();
    }


    @Rule
    public TestRule testWatcher = new TestWatcher() {
        @Override
        public Statement apply(Statement base, Description description) {
            return super.apply(base, description);
        }


        @Override
        protected void failed(Throwable e, Description description) {
            testSt.outGrade(description.getMethodName(), -1.0);
        }
    };

    @Test
    public void TestSimple_1(){
        String comentario = "Prueba simple: push por la izquierda y pop por la derecha y viceversa";
        DequeList<String> s = new DequeList<String>();
        String[] test = {"hola", "qué", "tal", "estás"};
        String[] resp = new String[10];

        for(int i = 0; i < test.length; i++){
            s.pushLeft(test[i]);
        }
        for(int i = 0; i < test.length; i++){
            resp[i] = s.popRight();
            Assert.assertEquals(comentario, test[i], resp[i]);
        }
        Assert.assertEquals(comentario, 0, s.size());

        for(int i = 0; i < test.length; i++){
            s.pushRight(test[i]);
        }
        for(int i = 0; i < test.length; i++){
            resp[i] = s.popLeft();
            Assert.assertEquals(comentario, test[i], resp[i]);
        }
        Assert.assertEquals(comentario, 0, s.size());

    }

    @Test
    public void TestSimple_2(){
        String comentario = "Prueba simple: push por la izquierda y pop por la izquierda, igual por la derecha";
        DequeList<String> s = new DequeList<String>();
        String[] test = {"hola", "qué", "tal", "estás"};
        String[] resp = new String[10];

        for(int i = 0; i < test.length; i++){
            s.pushLeft(test[i]);
        }
        for(int i = 0; i < test.length; i++){
            resp[i] = s.popLeft();
            Assert.assertEquals(comentario, test[test.length -  i - 1], resp[i]);
        }
        Assert.assertEquals(comentario, 0, s.size());

        for(int i = 0; i < test.length; i++){
            s.pushRight(test[i]);
        }
        for(int i = 0; i < test.length; i++){
            resp[i] = s.popRight();
            Assert.assertEquals(comentario, test[test.length -  i - 1], resp[i]);
        }
        Assert.assertEquals(comentario, 0, s.size());

    }

    @Test
    public void TestComp(){
/*        String comentario = "Prueba con objetos compuestos";
        ArrayList<Stack> test;
        Stack stack;
        DequeList<ArrayList> s  = new DequeList<ArrayList>();
        DequeListRef<ArrayList> r  = new DequeListRef<ArrayList>();

        for(int i = 0;i < 5; i++) {
            test = new ArrayList<>();
            for (int j = 0; j < 16; j++) {
                stack = new Stack<String>();
                for (int k = 0; k < 10; k++) {
                    stack.push("k=" + k);
                }
                test.add(stack);
            }
            if(i % 2 == 0) {
                s.pushLeft(test);
                r.pushLeft(test);
                Assert.assertEquals(comentario, s.size(), r.size());
            }
            else{
                s.pushRight(test);
                r.pushRight(test);
                Assert.assertEquals(comentario, s.size(), r.size());
            }
        }

        int i = 0;
        while(r.size() > 0){
            if(i % 2 == 0){
                Assert.assertEquals(comentario, s.popRight(), r.popRight());
                Assert.assertEquals(comentario, s.size(), r.size());
            }else{
                Assert.assertEquals(comentario, s.popLeft(), r.popLeft());
                Assert.assertEquals(comentario, s.size(), r.size());
            }
            i++;
        }*/

    }
    @Test
    public void TestSize(){
        String comentario = "Prueba de uso de memoria";
        DequeList<String> s = new DequeList<String>();
        //String[] test = new String[25];

        for(int i = 0; i < 50; i++){
            s.pushRight("0");
        }

        Assert.assertEquals(comentario, 50, s.mem());

        for(int i = 50; i > 15; i--){
            s.popLeft();
        }
        Assert.assertEquals(comentario, 15, s.mem());

    }

    @Test
    public void TestIterator(){
        String comentario = "Prueba de uso de memoria";
        DequeList<String> s = new DequeList<String>();
        Iterator iter;
        int n = 0;

        for(int i = 0; i < 50; i++){
            s.pushRight("0");
        }

        iter = s.iterator();
        while(iter.hasNext()){
            iter.next();
            n++;
        }
        Assert.assertEquals(comentario, n, s.mem());
    }

    @Test
    public void TestMemory(){
        String comentario = "Prueba de uso de memoria";
        DequeList<String> s = new DequeList<String>();
        Iterator iter;
        int n = 0;

        for(int i = 0; i < 50; i++){
            s.pushRight(String.valueOf(i));
        }

        for(int i = 50; i > 15; i--){
            s.popLeft();
        }

        iter = s.iterator();
        while(iter.hasNext()){
            iter.next();
            n++;
        }
        Assert.assertEquals(comentario, n, s.mem());

        for(int i = 15; i > 5; i--){
            s.popRight();
        }

        iter = s.iterator();
        n = 0;
        while(iter.hasNext()){
            iter.next();
            n++;
        }
        Assert.assertEquals(comentario, n, s.mem());
    }

}