package examenes.a2223.ord.parcial1.a;

import examenes.a2223.ord.parcial1.a.Movement;
import org.junit.*;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.rules.Timeout;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class MovementTest {
    @Rule
    public Timeout globalTimeout = new Timeout(80000);


    static public class TestState{
        BufferedWriter grade;
        TestState() {
            try{
                grade = new BufferedWriter(new FileWriter("results/parcial1.a", false));
                grade.write("@@@\tTest\t5\n");
            }catch (IOException e) {
                AssertionError ae = new AssertionError(e);
                throw ae;
            }
        }
        public void done() {
            try{
                grade.close();
            }catch (IOException e) {
                AssertionError ae = new AssertionError(e);
                throw ae;
            }
        }
        public void outGrade(String region, double g) {
            try{
                grade.write("@@@ "+"\t"+region+"\t"+g+"\n");
            }catch (IOException e) {
                AssertionError ae = new AssertionError(e);
                throw ae;
            }
        }
    }
    static TestState testSt;
    @BeforeClass
    public static void beforeTest() {
        testSt = new TestState();
    }
    @AfterClass
    public static void afterTest() {
        testSt.done();
    }


    @Rule
    public TestRule testWatcher = new TestWatcher() {
        @Override
        public Statement apply(Statement base, Description description) {
            return super.apply(base, description);
        }


        @Override
        protected void failed(Throwable e, Description description) {
            switch(description.getMethodName()){
                case "Test_toString":
                case "Test_gets":
                case "Test_sets":
                case "Test_isValid":
                case "Test_equals":
                case "":
                    testSt.outGrade("TEST", -1.0);
                    break;
                default:
                    break;
            }
        }
    };


    @Test
    public void Test_toString() {
        double speed, angle, time;

        speed = 0.1;
        angle = 0.3;
        time = 2.4;

        Movement mov = new Movement(speed, angle, time);
        String expected = "[0.1 - 0.3 - 2.4]";
        Assert.assertEquals("Error in toString", expected, mov.toString());

    }

    @Test
    public void Test_gets() {
        double speed, angle, time;

        speed = 0.1;
        angle = 0.3;
        time = 2.4;

        Movement mov = new Movement(speed, angle, time);

        Assert.assertEquals("Error in getSpeed", speed, mov.getSpeed(), 0.0);
        Assert.assertEquals("Error in getAngle", angle, mov.getAngle(), 0.0);
        Assert.assertEquals("Error in getDuration", time, mov.getDuration(), 0.0);

    }

    @Test
    public void Test_sets() {
        double speed, angle, time;

        speed = 0.0;
        angle = 0.0;
        time = 0.0;

        Movement mov = new Movement(speed, angle, time);
        mov.setSpeed(0.1);
        mov.setAngle(0.2);
        mov.setDuration(2.4);

        Assert.assertEquals("Error in setSpeed", 0.1, mov.getSpeed(), 0.0);
        Assert.assertEquals("Error in setAngle", 0.2, mov.getAngle(), 0.0);
        Assert.assertEquals("Error in setTime", 2.4, mov.getDuration(), 0.0);

    }

    @Test
    public void Test_isValid() {
        double speed, angle, time;

        speed = 0.1;
        angle = 0.0;
        time = 2.4;
        Movement mov = new Movement(speed, angle, time);
        Assert.assertEquals("Error in isValid (speed)", true, mov.isValid());

        speed = 1.6;
        angle = 0.0;
        time = 2.4;
        mov = new Movement(speed, angle, time);
        Assert.assertEquals("Error in isValid (speed)", false, mov.isValid());

        speed = 0.1;
        angle = Math.PI/2 + 0.1;
        time = 2.4;
        mov = new Movement(speed, angle, time);
        Assert.assertEquals("Error in isValid (angle)", false, mov.isValid());

        speed = 0.1;
        angle = 0.0;
        time = -2.4;
        mov = new Movement(speed, angle, time);
        Assert.assertEquals("Error in isValid (time)", false, mov.isValid());

        speed = 5.2;
        angle = 3.14;
        time = -2.4;
        mov = new Movement(speed, angle, time);
        Assert.assertEquals("Error in isValid", false, mov.isValid());

    }

    @Test
    public void Test_equals() {
        double speed, angle, time;

        speed = 0.1;
        angle = 0.3;
        time = 2.4;

        Movement mov1 = new Movement(speed, angle, time);
        Movement mov2 = new Movement(speed, angle, time);


        Assert.assertEquals("Error in equals", true, mov1.equals(mov2));

        mov2 = new Movement(speed, angle, time + 0.1);
        Assert.assertEquals("Error in equals", false, mov1.equals(mov2));

    }

}