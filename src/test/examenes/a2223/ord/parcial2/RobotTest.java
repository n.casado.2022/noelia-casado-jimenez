package examenes.a2223.ord.parcial2;

import org.junit.*;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.rules.Timeout;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.io.*;
import java.util.ArrayList;
import java.util.Deque;

public class RobotTest {
    @Rule
    public Timeout globalTimeout = new Timeout(80000);
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }


    public void ioError(String test_name, String expected, String printed) {
        System.err.println("Expected:");
        System.err.print(expected);
        System.err.print(" \n----");
        System.err.println("\nFound:");
        System.err.println(printed);
    }


    static public class TestState{
        BufferedWriter grade;

        TestState() {
            try{
                grade = new BufferedWriter(new FileWriter("results/parcial2.c", false));
                grade.write("@@@\tTest\t7\n");
            }catch (IOException e) {
                AssertionError ae = new AssertionError(e);
                throw ae;
            }
        }
        public void done() {
            try{
                grade.close();
            }catch (IOException e) {
                AssertionError ae = new AssertionError(e);
                throw ae;
            }
        }
        public void outGrade(String region, double g) {
            try{
                grade.write("@@@ "+"\t"+region+"\t"+g+"\n");
            }catch (IOException e) {
                AssertionError ae = new AssertionError(e);
                throw ae;
            }
        }
    }
    static TestState testSt;
    @BeforeClass
    public static void beforeTest() {
        testSt = new TestState();
    }
    @AfterClass
    public static void afterTest() {
        testSt.done();
    }




    @Rule
    public TestRule testWatcher = new TestWatcher() {
        @Override
        public Statement apply(Statement base, Description description) {
            return super.apply(base, description);
        }


        @Override
        protected void failed(Throwable e, Description description) {
            switch(description.getMethodName()){
                case "Test_navigate_1":
                case "Test_navigate_2":
                case "Test_getStringWPs":
                case "Test_getRemainingWPs":
                case "Test_main":
                case "Test_main_nav_error":
                case "Test_main_skip_points":
                case "":
                    testSt.outGrade("TEST", -1.0);
                    break;
                default:
                    break;
            }
        }
    };


    @Test
    public void Test_navigate_1() {

        String expected_fwd = "** test ** - navigating FORWARD\n" +
                "[0.4, 0.1]: 0.4123105625617661\n" +
                "[0.1, 0.5]: 0.5099019513592785\n";

        String expected = expected_fwd + "** test ** - navigating REVERSE\n" +
                "[0.1, 0.5]: 0.5099019513592785\n" +
                "[0.4, 0.1]: 0.4123105625617661\n";

        Waypoint w = new Waypoint(0.1, 0.2, 0.3, 0.4);
        ArrayList<Waypoint> wps = new ArrayList<>();
        wps.add(new Waypoint(0.1, 0.5));
        wps.add(new Waypoint(0.4, 0.1));

        Robot r = new Robot("test", wps);
        r.navigateForward(2);

        r = new Robot("test", wps);
        r.navigateReverse(2);

        Assert.assertEquals(expected, outContent.toString());

        r = new Robot("test", wps);
        Assert.assertFalse(r.navigateForward(3));
        r = new Robot("test", wps);
        Assert.assertTrue(r.navigateForward(1));

    }

    @Test
    public void Test_navigate_2() {

        String expected = "** test ** - navigating REVERSE\n" +
                "[0.2, 0.6]: 0.6324555320336759\n" +
                "** test ** - navigating FORWARD\n" +
                "[0.1, 0.0]: 0.1\n" +
                "[0.0, 0.4]: 0.4\n" +
                "[0.4, 0.1]: 0.4123105625617661\n" +
                "** test ** - navigating FORWARD\n" +
                "[0.1, 0.5]: 0.5099019513592785\n";


        Waypoint w = new Waypoint(0.1, 0.2, 0.3, 0.4);
        ArrayList<Waypoint> wps = new ArrayList<>();
        wps.add(new Waypoint(0.1, 0.5));
        wps.add(new Waypoint(0.4, 0.1));
        wps.add(new Waypoint(0.0, 0.4));
        wps.add(new Waypoint(0.2, 0.6));
        wps.add(new Waypoint(0.1, 0.0));

        Robot r = new Robot("test", wps);
        r.navigateReverse(1);
        r.navigateForward(3);
        r.navigateForward(1);

        Assert.assertEquals(expected, outContent.toString());

    }


    @Test
    public void Test_getStringWPs() {
        String expected = "** test ** - 3 WPs to visit\n" +
                ">>> [0.0, 0.4]: 0.4 <--> [0.4, 0.1]: 0.4123105625617661 <--> [0.1, 0.5]: 0.5099019513592785 <<<";
        Waypoint w = new Waypoint(0.1, 0.2, 0.3, 0.4);
        ArrayList<Waypoint> wps = new ArrayList<>();
        wps.add(new Waypoint(0.1, 0.5));
        wps.add(new Waypoint(0.4, 0.1));
        wps.add(new Waypoint(0.0, 0.4));

        Robot r = new Robot("test", wps);

        Assert.assertEquals(expected, r.getStringWPs());
    }

    @Test
    public void Test_getRemainingWPs() {
        Waypoint w = new Waypoint(0.1, 0.2, 0.3, 0.4);
        ArrayList<Waypoint> wps = new ArrayList<>();
        wps.add(new Waypoint(0.1, 0.5));
        wps.add(new Waypoint(0.4, 0.1));
        wps.add(new Waypoint(0.0, 0.4));
        wps.add(new Waypoint(0.2, 0.6));
        wps.add(new Waypoint(0.1, 0.0));

        Robot r = new Robot("test", wps);

        Assert.assertEquals(5, r.getRemainingWPs());

        r.navigateForward(4);
        Assert.assertEquals(1, r.getRemainingWPs());

        r.navigateForward(1);
        Assert.assertEquals(0, r.getRemainingWPs());


    }


    @Test
    public void Test_main() {

        String expected = "** r2d2 ** - 3 WPs to visit\n" +
                ">>> [0.0, 0.1]: 0.0 <--> [0.4, 0.1]: 0.4 <--> [0.1, 0.5]: 0.4123105625617661 <<<\n" +
                "** r2d2 ** - navigating FORWARD\n" +
                "[0.0, 0.1]: 0.0\n" +
                "[0.4, 0.1]: 0.4\n" +
                "** r2d2 ** - 1 WPs to visit\n" +
                ">>> [0.1, 0.5]: 0.4123105625617661 <<<\n";

        String file = "resources/examenes/wps.txt";
        String args[] = new String[5];
        args[0] = file;
        args[1] = "2";
        args[2] = "0.0";
        args[3] = "0.1";
        args[4] = "forward";

        Robot.main(args);

        Assert.assertEquals(expected, outContent.toString());
    }

    @Test
    public void Test_main_nav_error() {

        String expected = "Navigation error\n";

        String file = "resources/examenes/wps.txt";
        String args[] = new String[5];
        args[0] = file;
        args[1] = "4";
        args[2] = "0.0";
        args[3] = "0.1";
        args[4] = "forward";

        Robot.main(args);

        Assert.assertEquals(expected, errContent.toString());

    }

    @Test
    public void Test_main_skip_points() {

        String expected = "** r2d2 ** - 2 WPs to visit\n" +
                ">>> [0.0, 0.1]: 0.0 <--> [0.1, 0.5]: 0.4123105625617661 <<<\n" +
                "** r2d2 ** - navigating FORWARD\n" +
                "[0.0, 0.1]: 0.0\n" +
                "[0.1, 0.5]: 0.4123105625617661\n" +
                "The robot reached the final WP\n";

        String file = "resources/examenes/wps_nok.txt";
        String args[] = new String[5];
        args[0] = file;
        args[1] = "2";
        args[2] = "0.0";
        args[3] = "0.1";
        args[4] = "forward";

        Robot.main(args);

        Assert.assertEquals(expected, outContent.toString());

    }
    @Test
    public void Test_getRoute() {

        String first = "[0.1, 0.0]: 0.1";
        String third = "[0.0, 0.4]: 0.4";
        String last = "[0.2, 0.6]: 0.6324555320336759";

        Waypoint w = new Waypoint(0.1, 0.2, 0.3, 0.4);
        ArrayList<Waypoint> wps = new ArrayList<>();
        wps.add(new Waypoint(0.1, 0.5));
        wps.add(new Waypoint(0.4, 0.1));
        wps.add(new Waypoint(0.0, 0.4));
        wps.add(new Waypoint(0.2, 0.6));
        wps.add(new Waypoint(0.1, 0.0));

        Robot r = new Robot("test", wps);

        Assert.assertTrue(r.getRoute() instanceof Deque || r.getRoute() instanceof examenes.a2223.ord.parcial2.Deque);

        if(r.getRoute() instanceof Deque){ // util
            Deque du = (Deque) r.getRoute();
            Assert.assertEquals(first, du.pollFirst().toString());
            Assert.assertEquals(last, du.pollLast().toString());

            Assert.assertEquals(third, du.pollFirst().toString());
        }else{ // custom
            examenes.a2223.ord.parcial2.Deque dc = (examenes.a2223.ord.parcial2.Deque) r.getRoute();
            Assert.assertEquals(first, dc.popLeft().toString());
            Assert.assertEquals(last, dc.popRight().toString());

            Assert.assertEquals(third, dc.popLeft().toString());
        }



    }


}