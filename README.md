# Algoritmos y Estructuras de Datos

## Configuración del entorno
1. Abre este proyecto en Intellij IDEA
2. Entra en *File > Project Strcucture > Modules* y asegúrate de que están correctamente marcados los directorios:
    - *Sources Folder*: *src/main/java*
    - *Test Sources Folder*: *src/test*
    - *Excluded Folder*: *target*
3. Añade la dependencia a *algs4.jar* en *File > Project Strcucture > Libraries*
4. Añade las dependencias de *junit* (pruebas unitarias) en *File > Project Strcucture > Libraries*  (utilizando la opción *from Maven...*)
