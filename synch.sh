#!/bin/sh

git remote add upstream https://gitlab.etsit.urjc.es/rperez/lab-ayedd
git fetch upstream
git checkout main
git rebase upstream/main
git push -f origin main
